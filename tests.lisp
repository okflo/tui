(in-package :tui)

#+ (or sbcl ccl)
(defvar *swank-server* nil)

#+ (or sbcl ccl)
(unless *swank-server*
  (setf *swank-server*
        (swank:create-server :port 5555
                             :dont-close t)))

(defparameter *test-debug* nil)

(defkeymap *test-01*
    (key)
  ((#. charms/ll:key_right) move-right
   (when (< (+ (begin-x (active-window *screen*))
               (width (active-window *screen*)))
            (width *screen*))
     (incf (begin-x (active-window *screen*)))))
  ((#. charms/ll:key_left) move-left
   (when (> (begin-x (active-window *screen*)) 0)
     (decf (begin-x (active-window *screen*)))))
  ((#. charms/ll:key_up) move-up
   (when (> (begin-y (active-window *screen*)) 0)
     (decf (begin-y (active-window *screen*)))))
  ((#. charms/ll:key_down) move-down
   (when (< (+ (begin-y (active-window *screen*))
               (height (active-window *screen*)))
            (height *screen*))
     (incf (begin-y (active-window *screen*)))))
  ((#. (char-code #\1)) active-1
   (set-active (window :erstes))
   ;; (repaint (window :erstes))
   )
  ((#. (char-code #\2)) active-2
   (set-active (window :zweites))
   ;; (repaint (window :zweites))
   )
  ((#. (char-code #\3)) active-3
   (set-active (window :drittes))
   ;; (repaint (window :drittes))
   )
  ((#. (char-code #\q)) quit
   (signal 'exit-event-loop)))

(defun test-01 ()
  (with-tui
    (make-instance 'window :name :erstes
                   :height 10 :width 40 :begin-y 1 :begin-x 1
                   :draw-func (lambda (self)
                                (wprint self 1 1 "Window 1 - without borders!")
                                (wprint self 1 3 "Press 1 to activate this window."))
                   :event-map *test-01*)
    (make-instance 'bordered-window :name :zweites
                   :height 10 :width 40 :begin-y 4 :begin-x 5
                   :draw-func (lambda (self)
                                (wprint self 1 1 "Window 2")
                                (wprint self 1 3 "Press 2 to activate this window."))
                   :event-map *test-01*)
    (make-instance 'bordered-window :name :drittes
                   :height 10 :width 40 :begin-y 7 :begin-x 15
                   :draw-func (lambda (self)
                                (wprint self 1 1 "Window 3")
                                (wprint self 1 3 "Press 3 to activate this window."))
                   :event-map *test-01*)
    (make-instance 'bordered-window :name :description
                   :begin-x 0 :begin-y 17 :width 78 :height 4
                   :draw-func (lambda (self)
                                (wprint self 1 1
                                        "Fixed windows: Press 1, 2, or 3 to activate window. Press 'q' to quit.")
                                (wprint self 1 2
                                        "Press cursor-up/down/left/right to move the active window.")))
    (set-active (window :drittes))
    (event-loop)))

(defclass my-custom-window (titled-window bordered-window)
  ())

(defun test-01a ()
  (with-tui
    (make-instance 'titled-window :name :erstes
                   :height 10 :width 40 :begin-y 1 :begin-x 1
                   :title "Borderless Window 1"
                   :draw-func (lambda (self)
                                (wprint self 1 1 "Window 1 - without borders!")
                                (wprint self 1 3 "Press 1 to activate this window."))
                   :event-map *test-01*)
    (setf *test-debug* (make-instance 'my-custom-window :name :zweites
                                      :height 10 :width 40 :begin-y 4 :begin-x 5
                                      :title "Window 2"
                                      :draw-func (lambda (self)
                                                   (wprint self 1 1 "Window 2")
                                                   (wprint self 1 3 "Press 2 to activate this window."))
                                      :event-map *test-01*))
    (make-instance 'my-custom-window :name :drittes
                   :height 10 :width 40 :begin-y 7 :begin-x 15
                   :title "Window 3"
                   :draw-func (lambda (self)
                                (wprint self 1 1 "Window 3")
                                (wprint self 1 3 "Press 3 to activate this window."))
                   :event-map *test-01*)
    (make-instance 'bordered-window :name :description
                   :begin-x 0 :begin-y 17 :width 78 :height 5
                   :draw-func (lambda (self)
                                (wprint self 1 1
                                        "This time with titles!")
                                (wprint self 1 2
                                        "Press 1, 2, or 3 to activate window. Press 'q' to quit.")
                                (wprint self 1 3
                                        "Press cursor-up/down/left/right to move the active window.")))
    (set-active (window :drittes))
    (event-loop )))

(defkeymap *test-02*
    (key)
  ((#. (char-code #\o)) toogle-active
   (if (active-p (window :resize-window-one))
       (set-active (window :resize-window-two))
       (set-active (window :resize-window-one)))
   (repaint (window :resize-window-one))
   (repaint (window :resize-window-two)))
  ((#. (char-code #\q)) quit
   (signal 'exit-event-loop)))

(defclass my-custom-window-02 (bordered-window resizable-window)
  ())

(defun test-02 ()
  (with-tui
    (setf *test-debug* (make-instance 'my-custom-window-02
                                      :name :resize-window-one
                                      :start-left-dist 0
                                      :start-top-dist 0
                                      :end-right-dist 30
                                      :end-bottom-dist 0
                                      :draw-func (lambda (self)
                                                   (wprint self 1 1
                                                           (format nil "This is ~A" (name self)))
                                                   (wprint self 1 3
                                                           "Try to resize you xterm (if you're under XWindow).")
                                                   (wprint self 1 4
                                                           "Type 'o' to toggle active window.")
                                                   (wprint self 1 6
                                                           "Press 'q' to exit"))
                                      :event-map *test-02*))
    (make-instance 'my-custom-window-02
                   :name :resize-window-two
                   :start-right-dist 30
                   :start-top-dist 0
                   :end-right-dist 0
                   :end-bottom-dist 0
                   :draw-func (lambda (self)
                                (wprint self 1 1
                                        (format nil "This is ~A" (name self))))
                   :event-map *test-02*)
    (set-active (window :resize-window-one))
    (event-loop)))

(defkeymap *test-03*
    (key)
  (((97 100)) insert-key
   (incf (key-az (window :event-test)))
   (repaint (window :event-test)))
  ((#. (char-code #\Can) #. (char-code #\o)) key-ctrl-x-o
   (incf (key-ctrl-x-o (window :event-test)) )
   (repaint (window :event-test)))
  
  ((#. (char-code #\q)) quit
   (signal 'exit-event-loop)))

(defclass window-test-03 (resizable-window titled-window bordered-window)
  ((key-az
    :accessor key-az
    :initform 0)
   (key-ctrl-x-o
    :accessor key-ctrl-x-o
    :initform 0)))

(defun test-03 ()
  (with-tui
    (make-instance 'window-test-03
                   :title "Event Test!"
                   :name :event-test
                   :start-left-dist 0
                   :start-top-dist 0
                   :end-right-dist 00
                   :end-bottom-dist 0
                   :draw-func (lambda (self)
                                (wprint self 1 1
                                        "This tests input:")
                                (wprint self 1 3
                                        (format nil "Press one of the keys a,b,c or d Currently pressed ~A times."
                                                (key-az self)))
                                (wprint self 1 5
                                        (format nil "Press CTRL+X following o. Currently pressed ~A times."
                                                (key-ctrl-x-o self)))
                                (wprint self 1 6
                                        "Also - this window has a title...")
                                (wprint self 1 8
                                        "Press 'q' to exit"))
                   :event-map *test-03*)
    (set-active (window :event-test))
    (event-loop)))

(defkeymap *test-04*
    (key)
  ((#. charms/ll:key_up) keyscroll-up
                         (scroll-up (window :buffered-window-test))
                         (repaint (window :buffered-window-test)))
  ((#. charms/ll:key_down) keyscroll-down
                           (scroll-down (window :buffered-window-test))
                                        ;(repaint (window :buffered-window-test))
                           )
  ((#. charms/ll:key_right) keyscroll-right
                            (scroll-right (window :buffered-window-test))
                            (repaint (window :buffered-window-test)))
  ((#. charms/ll:key_left) keyscroll-left
                           (scroll-left (window :buffered-window-test))
                           (repaint (window :buffered-window-test)))
  ((#. (char-code #\esc) #. (char-code #\<)) scroll-beginning
                                             (scroll-up-to-begining (window :buffered-window-test))
                                             (repaint (window :buffered-window-test)))
  ((#. (char-code #\esc) #. (char-code #\>)) scroll-ending
                                             (scroll-down-to-end (window :buffered-window-test))
                                             (repaint (window :buffered-window-test)))
  ((#. (char-code #\Soh)) scroll-left-begining
                          (scroll-to-left-begining (window :buffered-window-test))
                          (repaint (window :buffered-window-test)))
  ((#. (char-code #\Enq)) scroll-right-begining
                          (scroll-to-right-end (window :buffered-window-test))
                          (repaint (window :buffered-window-test)))
  ((#. (char-code #\q)) quit
                        (signal 'exit-event-loop)))

(defclass window-test-04 (bordered-window resizable-window buffered-window titled-window )
  ())

(defun test-04 ()
  (with-tui
    (make-instance 'window-test-04
                   :title "Showing buffered window..."
                   :name :buffered-window-test
                   :start-left-dist 0
                   :start-top-dist 0
                   :end-right-dist 0
                   :end-bottom-dist 7
                   :event-map *test-04*)
    (make-instance 'resizable-window
                   :name :description
                   :start-left-dist 0
                   :start-bottom-dist 7
                   :end-right-dist 0
                   :end-bottom-dist 0
                   :draw-func (lambda (self)
                                (wprint
                                 self 1 1
                                 "Press cursor key up/down/left/right to scroll...")
                                (wprint
                                 self 1 2
                                 "Press ctrl-a and ctrl-e or alt-< and alt->.")
                                (wprint self 1 4 "Press 'q' to quit.")))
    (wprint (window :buffered-window-test) 4 1 "Buffered window demo: Let's scroll and scroll! :)")
    (loop for i from 3 to 50
       do
         (wprint (window :buffered-window-test) 1 i
                 (format nil "Line: ~A. That's a text." i)))
    (loop
       for i from 52 to 180
       do
         (wprint (window :buffered-window-test) 1 i
                 (format nil "Line: ~A. And some more text, so you'll have to scroll left/right. And now for something completely different... Well sir, I have a silly walk and I'd like to obtain a Government grant to help me develop it. ~{ ~A ~}" i (loop for ii from 52 to i collect ii))))
    (repaint (window :buffered-window-test))
    (set-active (window :buffered-window-test))
    (event-loop)))

(defkeymap *test-04a*
    (key)
  ((#. (char-code #\q)) quit
   (signal 'exit-event-loop)))

(defclass window-test-04a (bordered-window titled-window centered-window)
  ())

(defun test-04a ()
  (with-tui
    (make-instance 'window-test-04a
                   :title "A centered window..."
                   :name :centered-test-window
                   :width 40
                   :height 10
                   :draw-func (lambda (self)
                                (wprint self 2 2
                                        "I am centered!")
                                (wprint self 2 4
                                        "Resize you xterm.")
                                (wprint self 2 6
                                        "Press <q> to quit."))
                   :event-map *test-04a*)
    (set-active (window :centered-test-window))
    (event-loop)))

(defclass window-test-05 (resizable-window titled-window widgeted-window bordered-window)
  ())

(defkeymap *test-05*
    (key)
  ((#. (char-code #\a)) activate-widget-a
   (activate-widget (window :resizable-window) (widget :resizable-window :firstname))
   (repaint (window :resizable-window))
   (refresh (window :resizable-window)))
  ((#. (char-code #\b)) activate-widget-b
   (activate-widget (window :resizable-window) (widget :resizable-window :lastname))
   (repaint (window :resizable-window))
   (refresh (window :resizable-window)))
  ((#. (char-code #\c)) activate-widget-c
   (activate-widget (window :resizable-window) (widget :resizable-window :anything))
   (repaint (window :resizable-window))
   (refresh (window :resizable-window)))
  ((#. (char-code #\q)) quit
   (signal 'exit-event-loop)))

(defun test-05 ()
  (with-tui
      (make-instance 'window-test-05
                     :name :resizable-window
                     :start-left-dist 0
                     :start-top-dist 0
                     :end-right-dist 0
                     :end-bottom-dist 0
                     :title "Widget test"
                     :draw-func (lambda (self)
                                  (wprint
                                   self 1 1
                                   "Press 'a', 'b', or 'c' to select input:")
                                  (wprint
                                   self 1 2
                                   "Once you have entered the input field, you can navigate forward (ctrl-f), backward (ctrl-b),")
                                  (wprint
                                   self 1 3
                                   "delete char under the cursor (ctrl-d), ore remove the char left of the cursor (backspace).")
                                  (wprint
                                   self 1 5
                                   "a: Please enter your firstname: ")
                                  (wprint
                                   self 1 6
                                   "b: Please enter your lastname: ")
                                  (wprint
                                   self 1 8
                                   "c: Please enter anything: ")
                                  (wprint
                                   self 1 10
                                   "Press 'q' to exit..."))
                     :event-map *test-05*)
    (make-instance 'text-field
                   :name :firstname
                   :x 33 :y 5
                   :width 40
                   :content "foo bar?"
                   :window (window :resizable-window))
    (make-instance 'text-field
                   :name :lastname
                   :x 33 :y 6
                   :width 40
                   :content "nothing"
                   :window (window :resizable-window))
    (make-instance 'auto-stretch-text-field
                   :name :anything
                   :x 33 :y 8
                   :dist-right 1
                   :content "What is it?"
                   :window (window :resizable-window))
    (set-active (window :resizable-window))
    (event-loop)))

(defparameter *menu*
  '(("File"
     ("Open..." "Ctrl-x f" nil)
     ("Save" "Ctrl-x s" nil)
     ("Write..." "Ctrl-x w" nil)
     (-)
     ("Quit" "Ctrl-x c" nil))
    ("Edit"
     ("Copy" "Meta-w" nil)
     ("Cut" "Ctrl-w" nil)
     ("Yank" "Ctr-y" nil))
    ("Test"
     ("Test 01" nil nil)
     ("Test 02" "Meta-2" test-menu-entry)
     ("Test 03 super long entry" nil nil)
     ("Test 04" nil nil)
     ("Test 05" nil nil)
     (-)
     ("Test 06" nil nil)
     ("Test 07 super super super long entry..." nil nil)
     ("Test 08" nil nil)
     ("Test 09" nil nil)
     ("Test 10" "-->" nil)
     ("Test 11" nil nil)
     ("Test 12" nil nil))
    ("Additional test entries"
     ("What shell we do with the drunken sailor?" "dizzy" nil)
     ("Help!" nil nil)
     ("I am the last unicorn..." nil nil))))

(defkeymap *test-06*
    (key)
  ((#. (char-code #\Esc) #. (char-code #\m))
   raise-menu
   (activate-widget (window :my-menu) (widget :my-menu :menu)))
  ((#. (char-code #\q))
   quit
   (signal 'exit-event-loop)))

(defclass window-test-06 (resizable-window titled-window widgeted-window)
  ((menu-entry-selected
    :accessor menu-entry-selected
    :initform nil)))

(defun test-menu-entry ()
  (setf (menu-entry-selected (window :my-menu))
        t))

(defun test-06 ()
  (with-tui
    (make-instance 'window-test-06
                   :name :my-menu
                   :start-left-dist 0
                   :start-top-dist 0
                   :end-right-dist 0
                   :end-bottom-dist 0
                   :title "Menu Test"
                   :draw-func (lambda (self)
                                (wprint self 1 1 "Press 'Meta-m' to activate the menubar.")
                                (wprint self 1 3 "Press 'q' to quit.")
                                (wprint self 1 4 "----------------------------------------------------------------------")

                                (when (menu-entry-selected self)
                                  (wprint self 1 5 "Menu entry selected!")))
                   :event-map *test-06*)
    (make-instance 'menu
                   :window (window :my-menu)
                   :name :menu
                   :menu-structure *menu*)
    (set-active (window :my-menu))
    (event-loop)))

(defclass window-test-07 (bordered-window widgeted-window centered-window titled-window)
  ((counter
    :initform 0
    :accessor counter)))

(defun test-07 ()
  (with-tui
    (make-instance 'window-test-07
                   :name :test-window
                   :title "Test buttons"
                   :height 15
                   :width 70
                   :draw-func (lambda (self)
                                (wprint self :centered 2 "Select a button with cursor-keys or <tab> and select/click")
                                (wprint self :centered 3 "them with <return>.")
                                (wprint self :centered 10 (format nil "Hit the Cancel-Button ~A times." (counter self)))))
    (make-instance 'button
                   :name :my-button
                   :window (window :test-window)
                   :x 11 :y 7
                   :content "Click me to proceed!"
                   :func-to-call-on-act (lambda (widget-self)
                                          (declare (ignore widget-self))
                                          (signal 'exit-event-loop)))
    (make-instance 'button
                   :name :cancel-button
                   :window (window :test-window)
                   :x 41 :y 7
                   :content "Cancel"
                   :func-to-call-on-act (lambda (widget-self)
                                          (declare (ignore widget-self))
                                          (incf (counter (window :test-window)))))
    (set-active (window :test-window))
    (activate-widget (window :test-window) (widget :test-window :cancel-button))
    (event-loop)))

(defun tests ()
  (with-tui
    (clear *screen*))
  (test-01)
  (test-01a)
  (test-02)
  (test-03)
  (test-04)
  (test-04a)
  (test-05)
  (test-06)
  (test-07))




