(in-package :cl)

(defpackage :net.diesenbacher.tui
  (:use :cl)
  (:nicknames :tui)
  (:export :screen
           :*screen*
           :init-screen
           :destroy
           :kill
           :clear
           :with-tui
           :mark-for-later-refresh
           :do-update
           :nodelay
           :repaint
           :refresh
           ;; windows
           :x :y :dist-right
           :window
           :active-window
           :scroll-up
           :scroll-down
           :scroll-left
           :scroll-right
           :scroll-up-to-begining
           :scroll-down-to-end
           :scroll-to-left-begining
           :scroll-to-right-end
           :height
           :width
           :bordered-window
           :titled-window
           :widgeted-window
           :resizable-window
           :buffered-window
           :centered-window
           :buffer
           :exit-event-loop
           :defkeymap
           :set-active
           :wprint
           :wput
           :wprint-append
           :with-wattr
           ;; widgets
           :widget
           :mother-window
           :activate-widget
           :deactivate-widget
           :text-field-widget
           :text-field
           :auto-stretch-text-field-widget
           :regex-validated-text-field-widget
           :select-item-widget
           :button
           :button-with-function-on-submit
           :select-box
           :selected-p
           :radio-button
           :content
           :menu
           ;; events
           :event-loop
           :event-maps
           :defkeymap
           :addkey
           :*text-field-widget-keymap*
           ))


