(in-package :tui)

(defkeymap *text-input-eventmap*
    (key)
  (((32 126))
   enter-char
   (when (< (1- (length (content (active-widget (active-window *screen*)))))
            (width (active-widget (active-window *screen*))))
     (setf (content (active-widget (active-window *screen*)))
           (concatenate 'string
                        (content (active-widget (active-window *screen*)))
                        (string (code-char key))))))
  ((#. charms/ll:key_backspace)
   backspace
   (when (> (length (content (active-widget (active-window *screen*))))
            0)
     (setf (content (active-widget (active-window *screen*)))
           (subseq  (content (active-widget (active-window *screen*)))
                    0
                    (- (length (content (active-widget (active-window *screen*))))
                       1)))))
  ((#. (char-code #\Newline))
   leave-widget
   (deactivate-widget (active-window *screen*))))

(defclass text-input (widget)
  ((width :initarg :width :accessor width :initform (error "width missing..."))
   (content :initarg :content :accessor content :initform "")
   (event-maps :initarg :event-maps :accessor event-maps
               :initform (list *text-input-eventmap*))))

(defmethod trim-content ((wi text-input))
  (string-fixed-size (content wi) (width wi)))

(defmethod paint-content ((wi text-input))
  (wprint (mother-window wi)
          (x wi) (y wi)
          (trim-content wi)))

(defmethod paint-cursor ((wi text-input))
  (with-wattr (mother-window wi) (charms/ll:a_reverse)
    (wput (mother-window wi)
          (+ (x wi) (length (content wi))) (y wi)
          " "))
  (wput (mother-window wi)
        (+ (x wi) (length (content wi)) 1)
        (y wi)
        " "))

(defmethod repaint ((wi text-input) &key)
  (paint-content wi)
  (when (active-p wi)
    (paint-cursor wi)))

(defkeymap *text-input-editable-eventmap*
    (key)
  (((32 126))
   insert-char
   (let ((wi (active-widget (active-window *screen*))))
     (let ((temp-content (copy-seq (content wi))))
       (setf temp-content
             (insert-char temp-content
                          (code-char key)
                          (+ (x-cursor-pos wi)
                             (x-offset wi))))
       (when (and (cl-ppcre:scan (validate-regex wi)
                                 temp-content)
                  (<= (length temp-content)
                      (max-content-length wi)))
         (setf (content wi) temp-content)
         (*text-field-event-map*-cursor-right key)))))

  ((#. (char-code #\Eot))
   delete-char
   (let ((wi (active-widget (active-window *screen*))))
     (when (> (length (content wi))
              (+ (x-cursor-pos wi)
                 (x-offset wi)))
       (setf (content wi)
             (remove-char
              (content wi)
              (+ (x-cursor-pos wi)
                 (x-offset wi)
                 1))))))
  
  ((or (#. (char-code #\Ack))
       (#. charms/ll:key_right))
   cursor-right
   (let ((wi (active-widget (active-window *screen*))))
     (cond
       ((and (< (x-cursor-pos wi)
                (1- (width wi)))
             (< (+ (x-cursor-pos wi)
                   (x-offset wi))
                (length (content wi))))
        (incf (x-cursor-pos wi)))
       ((and (= (x-cursor-pos wi)
                (1- (width wi)))
             (< (+ (x-offset wi)
                   (width wi))
                (1+ (length (content wi)))))
        (incf (x-offset wi))))))

  ((or (#. (char-code #\Stx))
       (#. charms/ll:key_left))
   cursor-left
   (let ((wi (active-widget (active-window *screen*))))
     (cond
       ((> (x-cursor-pos wi) 0)
        (decf (x-cursor-pos wi)))
       ((and (= (x-cursor-pos wi) 0)
             (> (x-offset wi) 0))
        (decf (x-offset wi))))))

  ((#. charms/ll:key_backspace)
   backspace-key
   (let ((wi (active-widget (active-window *screen*))))
     (when (> (+ (x-cursor-pos wi)
                 (x-offset wi))
              0)
       (setf (content wi)
             (remove-char
              (content wi)
              (+ (x-cursor-pos wi)
                 (x-offset wi))))
       (if (and (= (+ (x-offset wi) (width wi)) (+ (length (content wi)) 2))
                (> (x-offset wi) 0))
           (decf (x-offset wi))
           (decf (x-cursor-pos wi))))))

  ((or (#. (char-code #\tab))
       (#. charms/ll:key_down)
       (#. (char-code #\So)))
   next-widget
   (select-next-widget (active-window *screen*)))
  
  ((or (#. charms/ll:key_btab)
       (#. charms/ll:key_up)
       (#. (char-code #\Dle)))
   prev-widget
   (select-previous-widget (active-window *screen*)))

  ((#. (char-code #\Newline))
   leave-input-line-mode
   (deactivate-widget (active-window *screen*)))  )

(defclass text-input-editable (text-input)
  ((max-content-length :initarg :max-content-length :accessor max-content-length :initform 500)
   (x-cursor-pos :initarg :x-cursor-pos :accessor x-cursor-pos :initform 0)
   (x-offset :initarg :x-offset :accessor x-offset :initform 0)
   (event-maps :initarg :event-maps :accessor event-maps
               :initform (list *text-input-editable-eventmap*))))

(defclass text-input-regex-validated (text-input)
  ((validate-regex :initarg :validate-regex :accessor validate-regex :initform ".*")))

(defclass text-input-embraced (text-input)
  ((border-char :initarg :border-char :accessor border-char :initform #\|)))

