(in-package :tui)

(defclass widget ()
  ((mother-window
    :accessor mother-window
    :documentation "The mother-window, is the window, in that a widget is painted.")
   (name
    :initarg :name
    :accessor name
    :documentation "Name of the widget instance as a keyword.")
   (x
    :initarg :x
    :accessor x
    :documentation "x-offset of the widget to be drawn in its mother-window.")
   (y
    :initarg :y
    :accessor y
    :documentation "y-offset of the widget to be drawn in its mother-window.")
   (event-map
    :initarg :event-map
    :accessor event-map
    :initform nil
    :documentation "deprecated - will go")
   (event-maps
    :initarg :event-maps
    :accessor event-maps
    :initform nil
    :documentation "List of event-maps. Additionally to their 'native'
    event-map, widget can have additional event-maps to override the
    native event-map or to give all widget on a window a common
    event-map, f.e. ctrl-g to cancel the dialogue.")
   (func-to-call-on-focus
    :initarg :func-to-call-on-focus
    :accessor func-to-call-on-focus
    :initform nil
    :documentation "Function called, when the widget gains
    focus (= becomes active).")
   (func-to-call-on-act
    :initarg :func-to-call-on-act
    :accessor func-to-call-on-act
    :initform nil
    :documentation "Function called, when the widget is used.")))

(defmethod initialize-instance :after ((wi widget) &key window add-event-map)
  (unless window
    (error "We need a window for a widget!"))
  (setf (mother-window wi) window)
  (setf (widget-stack window)
        (append (widget-stack window)
                (list wi)))
  (when add-event-map
    (setf (event-maps wi)
          (cons add-event-map (event-maps wi))))
  (repaint (mother-window wi))
  (mark-for-later-refresh (mother-window wi)))

(defmethod activate-widget ((w widgeted-window) (wi widget))
  (setf (active-window *screen*) w)
  (setf (active-widget w) wi)
  (when (func-to-call-on-focus wi)
    (funcall (func-to-call-on-focus wi) wi)))

(defmethod deactivate-widget ((w widgeted-window))
  (setf (active-widget w) nil)
  (repaint w))

(defmethod destroy ((wi widget))
  (setf (widget-stack (mother-window wi))
        (remove wi (widget-stack (mother-window wi)) :test #'equal)))

(defmethod active-p ((wi widget))
  (equal wi
	 (active-widget (mother-window wi))))

(defun widget (window-name widget-name)
  (loop
    for i in (widget-stack (window window-name))
    when (equal widget-name (name i))
      do (return i)
    finally (error "Widget ~A in window ~A not found." widget-name window-name)))

(defun select-next-widget (window)
  (let ((pos-current-widget (position (active-widget window)
                                      (widget-stack window) :test #'equal)))
    (aif (nth (1+ pos-current-widget) (widget-stack window))
         (activate-widget window it)
         (activate-widget window (car (widget-stack window))))))

(defun select-previous-widget (window)
  (let ((pos-current-widget (position (active-widget window)
                                      (widget-stack window) :test #'equal)))
    (if (= pos-current-widget 0)
        (activate-widget window (car (last (widget-stack window))))
        (activate-widget window (nth (1- pos-current-widget) (widget-stack window))))))










