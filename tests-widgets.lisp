(in-package :tui)

(defclass my-window (titled-window bordered-window
                     widgeted-window resizable-window)
  ())

(defkeymap *simple-text-field-test*
    (key)
  ((#. (char-code #\a)) activate-widget1-simple-text
                        (activate-widget (window :my-window)
                                         (widget :my-window :test-widget1))
                        (repaint (window :my-window))
                        (refresh (window :my-window)))
  ((#. (char-code #\b)) activate-widget2-simple-text
                        (activate-widget (window :my-window)
                                         (widget :my-window :test-widget2)))
  ((#. (char-code #\c)) activate-widget3-simple-text
                        (activate-widget (window :my-window)
                                         (widget :my-window :test-widget3))
                        (repaint (window :my-window))
                        (refresh (window :my-window)))
  ((#. (char-code #\q)) quit
                        (signal 'exit-event-loop)))

(defun test-simple-text-field ()
  (with-tui
    (make-instance 'my-window
                   :title "Simple Text Field"
                   :name :my-window
                   :start-left-dist 0
                   :start-top-dist 0
                   :end-right-dist 0
                   :end-bottom-dist 0
                   :draw-func (lambda (self)
                                (wprint self 1 1
                                        "Test Simple Text Field:")
                                (wprint self 1 2
                                        "Press a/b/c to enter the field. Submit with Return.")
                                (wprint self 1 3
                                        "Press q to end demo.")
                                (wprint self 1 5
                                        "a: Enter your name: ")
                                (wprint self 1 6
                                        "b: Enter your address: ")
                                (wprint self 1 7
                                        "c: Enter your hobbies: "))
                   :event-map *simple-text-field-test*)
    (make-instance 'simple-text-field
                   :window (window :my-window)
                   :content "Teste..."
                   :width 15
                   :y 5
                   :x 21
                   :name :test-widget1)
    (make-instance 'simple-text-field
                   :window (window :my-window)
                   :content ""
                   :width 15
                   :y 6
                   :x 24
                   :name :test-widget2)
    (make-instance 'simple-text-field
                   :window (window :my-window)
                   :content ""
                   :width 15
                   :y 7
                   :x 24
                   :name :test-widget3)
    (set-active (window :my-window))
    (event-loop )))

(defclass my-window2 (titled-window bordered-window
                      widgeted-window resizable-window)
  ())


(defkeymap *test-input-field*
    (key)
  ((#. (char-code #\a)) activate-widget-input-field
                        (activate-widget (window :my-window2)
                                         (widget :my-window2 :test-input-field))
                        (repaint (window :my-window2))
                        (refresh (window :my-window2)))
  ((#. (char-code #\q)) quit
                        (signal 'exit-event-loop)))

(defun test-input-field ()
  (with-tui
      (make-instance 'my-window2
                     :title "Simple Text Field"
                     :name :my-window2
                     :start-left-dist 0
                     :start-top-dist 0
                     :end-right-dist 0
                     :end-bottom-dist 0
                     :draw-func (lambda (self)
                                  (wprint self 1 1
                                          "Test input-field")
                                  (wprint self 1 2
                                          "Press a to enter the field. Submit with Return.")
                                  (wprint self 1 3
                                          "Press q to end demo."))
                     :event-map *test-input-field*)
    (make-instance 'input-field
                   :window (window :my-window2)
                   :name :test-input-field
                   :x 4
                   :y 10
                   :width 20
                   :height 4
                   :content #("Das ist ein" "  Test" "" ""))
    (set-active (window :my-window2))
    (event-loop )))


(defclass text-input-test-window (titled-window bordered-window widgeted-window resizable-window)
  ())

(defkeymap *test-text-input-eventmap*
    (key)
  ((#. (char-code #\a))
   activate-input1
   (activate-widget (window :text-input-test-window)
                    (widget :text-input-test-window :text-input-test-input)))
  ((#. (char-code #\b))
   activate-input2
   (activate-widget (window :text-input-test-window)
                    (widget :text-input-test-window :text-input-test-input-editable)))
  ((#. (char-code #\q))
   quit
   (signal 'exit-event-loop)))

(defun test-text-input ()
  (with-tui
    (make-instance 'text-input-test-window
                   :name :text-input-test-window
                   :title "Testing text-input..."
                   :start-left-dist 0
                   :start-top-dist 0
                   :end-right-dist 0
                   :end-bottom-dist 0
                   :draw-func (lambda (self)
                                (wprint self 2 2 "Testing the text-input widget...")
                                (wprint self 2 5 "Please input (type text-input):")
                                (wprint self 2 7 "Please input (type text-input-editable):"))
                   :event-map *test-text-input-eventmap*)
    (make-instance 'text-input
                   :name :text-input-test-input
                   :window (window :text-input-test-window)
                   :x 34
                   :y 5
                   :width 40)
    (make-instance 'text-input-editable
                   :name :text-input-test-input-editable
                   :window (window :text-input-test-window)
                   :x 43
                   :y 7
                   :width 40)
    (set-active (window :text-input-test-window))
    (event-loop)))





