(in-package :tui)

(declaim (optimize (speed 0) (debug 3)))

#+ (or sbcl ccl)
(defun dslime (text)
  (print text (slot-value (car swank::*connections*) 'swank::USER-OUTPUT)))

(defparameter *debug* nil)

(defclass window ()
  ((curses-handler
    :initarg :curses-handler
    :accessor curses-handler
    :initform (cffi:null-pointer))
   (name
    :initarg :name
    :reader name)
   (begin-x
    :initarg :begin-x
    :accessor begin-x
    :initform nil)
   (begin-y
    :initarg :begin-y
    :accessor begin-y
    :initform nil)
   (width
    :initarg :width
    :accessor width
    :initform nil)
   (height
    :initarg :height
    :accessor height
    :initform nil)
   (draw-func
    :initarg :draw-func
    :reader draw-func
    :initform nil)
   (init-hook
    :accessor init-hook
    :initform nil
    :documentation "Functions run, when an initialization of the
    window is required (f.e. first time instance, repaint of whole
    screen), specified following: '(function-name function-order).")
   (meta-hook
    :accessor meta-hook
    :initform (list)
    :documentation "Holds all window-class specific draw and logic
    functions as a list, specified following: '(function-name
    function-order). When the repaint-method is called, these
    functions are called, ordered by function-order.")
   (event-map
    :initarg :event-map
    :accessor event-map
    :initform nil)))

;; When begin-x, begin-y, width or height of a window changes, we need
;; a full repaint of the screen. Will be triggered by the event-loop.
(defmethod (setf begin-x) :after (new-value (w window))
  (declare (ignore new-value))
  (setf (need-full-repaint *screen*) t))

(defmethod (setf begin-y) :after (new-value (w window))
  (declare (ignore new-value))
  (setf (need-full-repaint *screen*) t))

(defmethod (setf width) :after (new-value (w window))
  (declare (ignore new-value))
  (setf (need-full-repaint *screen*) t))

(defmethod (setf height) :after (new-value (w window))
  (declare (ignore new-value))
  (setf (need-full-repaint *screen*) t))

(defun %init-func-window (w)
  (when (and (slot-boundp w 'curses-handler)
             (curses-handler w))
    (destroy w))
  (setf (curses-handler w) (charms/ll:newwin
                            (height w) (width w) 
                            (begin-y w) (begin-x w))))

(defun %meta-func-window (w)
  (when (draw-func w)
    (funcall (draw-func w) w)))

(defmethod initialize-instance :after ((w window) &key)
  (push w (window-stack *screen*))
  (push '(%meta-func-window 60) (meta-hook w))
  (push '(%init-func-window 100) (init-hook w)))

(defmethod initialize-instance :around ((w window) &key)
  (call-next-method)
  ;; do - really after everything else happened - the actual
  ;; initialization.
  (repaint w :do-initialization t))

(defclass bordered-window (window)
  ((verchar
    :initarg :verchar
    :accessor verchar
    :initform 0)
   (horchar
    :initarg :horchar
    :accessor horchar
    :initform 0)))

(defun %meta-func-bordered-window (w)
  (if (active-p w)
      (charms/ll:box (curses-handler w)
                     (verchar w) (horchar w))    
      (with-wattr w ((charms/ll:color-pair 2))
        (charms/ll:box (curses-handler w)
                       (verchar w) (horchar w)))))

(defmethod initialize-instance :after ((w bordered-window) &key)
  (push '(%meta-func-bordered-window 30) (meta-hook w)))

(defmethod active-p ((w window))
  (equal (active-window *screen*)
         w))

(defmethod box ((w bordered-window))
  (charms/ll:box (curses-handler w)
                 (verchar w) (horchar w)))

(defmethod set-active ((w window) &key (repaint-inactive t))
  (let ((previous-active (active-window *screen*)))
    (setf (active-window *screen*) w)
    (setf (window-stack *screen*)
          (cons w (loop for i in (window-stack *screen*)
                     unless (active-p i)
                     collect i)))
    (when previous-active
      (repaint previous-active)
      (mark-for-later-refresh previous-active)))
  ;; (decorate w)
  (repaint w)
  (mark-for-later-refresh (active-window *screen*)))

(defmethod destroy ((w window))
  (charms/ll:delwin (curses-handler w)))

(defmethod kill ((w window))
  (destroy w)
  (setf (window-stack *screen*)
        (remove w (window-stack *screen*)
                :test #'equal)))

(defmethod clear ((w window))
  (charms/ll:wclear (curses-handler w)))

(defmethod erase ((w window))
  (charms/ll:werase (curses-handler w)))

(defmethod refresh ((w window))
  (charms/ll:wrefresh (curses-handler w)))

(defmethod mark-for-later-refresh ((w window))
  (charms/ll:wnoutrefresh (curses-handler w)))

(define-condition tui-error (error)
  ())

(define-condition window-not-found (tui-error)
  ())

(defun window (window-name)
  (loop for i in (window-stack *screen*)
     when (equal window-name (name i))
     do (return i)
     finally (error "Window ~A not found." window-name)))

(defclass titled-window (window)
  ((title
    :initarg :title
    :accessor title
    :initform "no-title")
   (title-print-function
    :initarg :title-print-function
    :accessor title-print-function
    :initform (lambda (title)
                (format nil "= ~A =" title)))))

(defun %meta-func-titled-window (w)
  (if (active-p w)
      (wprint w 1 0
              (funcall (title-print-function w) (title w))
              :raw t)
      (with-wattr w ((charms/ll:color-pair 2))
        (wprint w 1 0
                (funcall (title-print-function w) (title w))
                :raw t)  )))

(defmethod initialize-instance :after ((w titled-window) &key)
  (push '(%meta-func-titled-window 40) (meta-hook w)))

(defclass widgeted-window (window)
  ((widget-stack
    :initarg :widget-stack
    :accessor widget-stack
    :initform nil)
   (active-widget
    :initarg :active-widget
    :accessor active-widget
    :initform nil)))

(defun %meta-func-widgeted-window (w)
  (loop for iwidget in (widget-stack w)
     do
       (repaint iwidget)))

(defmethod initialize-instance :after ((w widgeted-window) &key)
  (push '(%meta-func-widgeted-window 70) (meta-hook w)))

(defclass centered-window (window)
  ())

(defun %init-func-centered-window (w)
  (with-slots (height width) w
    (when (and (> (+ 2 (height *screen*))
                  height)
               (> (+ 2 (width *screen*))
                  width))
      (setf (begin-x w)
            (round (/ (- (width *screen*) width) 2))
            (begin-y w)
            (round (/ (- (height *screen*) height) 2))))))

(defmethod initialize-instance :after ((w centered-window) &key)
  (push '(%init-func-centered-window 20) (init-hook w)))

(defclass resizable-window (window)
  ((start-left-dist
    :initarg :start-left-dist
    :accessor start-left-dist
    :initform nil)
   (start-top-dist
    :initarg :start-top-dist
    :accessor start-top-dist
    :initform nil)
   (start-bottom-dist
    :initarg :start-bottom-dist
    :accessor start-bottom-dist
    :initform nil)
   (start-right-dist
    :initarg :start-right-dist
    :accessor start-right-dist
    :initform nil)
   (end-left-dist
    :initarg :end-left-dist
    :accessor end-left-dist
    :initform nil)
   (end-top-dist
    :initarg :end-top-dist
    :accessor end-top-dist
    :initform nil)
   (end-bottom-dist
    :initarg :end-bottom-dist
    :accessor end-bottom-dist
    :initform nil)
   (end-right-dist
    :initarg :end-right-dist
    :accessor end-right-dist
    :initform nil)))

(defun calculate-window-dimensions (&key start-left-dist start-top-dist
                                      start-bottom-dist start-right-dist
                                      end-left-dist end-top-dist
                                      end-bottom-dist end-right-dist)
  (let (begin-x begin-y width height)
    (if start-left-dist
        (setf begin-x start-left-dist)
        (setf begin-x (if (> (- (width *screen*) start-right-dist) 0)
                          (- (width *screen*) start-right-dist)
                          start-right-dist)))
    (if start-top-dist
        (setf begin-y start-top-dist)
        (setf begin-y (if (> (- (height *screen*) start-bottom-dist) 0)
                          (- (height *screen*) start-bottom-dist)
                          start-bottom-dist)))

    (if end-left-dist
        (setf width (- begin-x end-left-dist))
        (setf width (- (- (width *screen*) end-right-dist) begin-x )))
    (if end-top-dist
        (setf height (- begin-y end-top-dist))
        (setf height (- (- (height *screen*) end-bottom-dist) begin-y)))

    (values begin-x begin-y width height)))

(defun %init-func-resizable-window (w)
  ;; calculate begin-x / width and begin-y / height depending on
  ;; *screen*, start-x/start-y, end-x/end-y and their sign.
  (multiple-value-bind (begin-x begin-y width height)
      (with-slots (start-left-dist start-top-dist start-bottom-dist end-right-dist start-right-dist end-left-dist end-top-dist end-bottom-dist)
          w
        (calculate-window-dimensions :start-left-dist start-left-dist
                                     :start-top-dist start-top-dist
                                     :start-bottom-dist start-bottom-dist
                                     :start-right-dist start-right-dist
                                     :end-left-dist end-left-dist
                                     :end-top-dist end-top-dist
                                     :end-bottom-dist end-bottom-dist
                                     :end-right-dist end-right-dist))
    (setf (begin-x w) begin-x
          (begin-y w) begin-y
          (width w) width
          (height w) height)))

(defmethod initialize-instance :after ((w resizable-window) &key)
  (push '(%init-func-resizable-window 10) (init-hook w)))

(defclass buffered-window (window)
  ((buffer
    :initarg :buffer
    :accessor buffer
    :initform (make-array 1000
                          :element-type 'string
                          :adjustable t
                          :fill-pointer 0))
   (buffer-offset-y
    :accessor buffer-offset-y
    :initform 0)
   (buffer-offset-x
    :accessor buffer-offset-x
    :initform 0)))

(defun %meta-func-buffered-window-decorate (w)
  (when (> (buffer-offset-y w) 0)
    (wprint w (- (width w) 13)
            0
            (format nil "^(~A)" (buffer-offset-y w))
            :raw t))
  (when (> (- (length (buffer w)) (buffer-offset-y w))
           (- (height w) 2))
    (wprint w (- (width w) 13) (1- (height w))
            (format nil "v(~A)"
                    (- (- (length (buffer w)) (- (height w) 2))
                       (buffer-offset-y w)))
            :raw t))
  (when (> (buffer-offset-x w) 0)
    (wprint w 1 (1- (height w))
            (format nil "<(~A)" (buffer-offset-x w))
            :raw t))
  (let ((left-overlap (calculate-amount-hidden-content-right w)))
    (when (> left-overlap 0)
      (wprint w (- (width w) (+ 5 (truncate (log left-overlap 10)))) (1- (height w))
              (format nil "(~A)>" left-overlap)
              :raw t))))

(defun %meta-func-buffered-window-content (w)
  ;; (erase w)
  ;; draw content
  (when (and (> (height w) 0)
             (> (width w) 0))
    (loop
       for index from (buffer-offset-y w) to (+ (buffer-offset-y w) (height w))
       for line across (safe-subseq (buffer w)
                                    (buffer-offset-y w)
                                    (if (< (+ (buffer-offset-y w) (height w))
                                           (length (buffer w)))
                                        (+ (buffer-offset-y w) (height w))
                                        (length (buffer w))))
       for line-num from 1 to (- (height w) 2)
       do
         (wprint w 1 line-num
                 (concatenate 'string
                              (safe-subseq line
                                           (buffer-offset-x w)
                                           (if (< (+ (buffer-offset-x w) (width w))
                                                  (length line))
                                               (+ (buffer-offset-x w) (width w))
                                               (length line)))
                              (with-output-to-string (s)
                                (loop
                                   for i from 1 to (- (+ (width w) (buffer-offset-x w))
                                                      (length line))
                                   do
                                     (princ " " s))))
                 :raw t))))

(defmethod initialize-instance :after ((w buffered-window) &key)
  (push '(%meta-func-buffered-window-content 15) (meta-hook w))
  (push '(%meta-func-buffered-window-decorate 35) (meta-hook w)))

(defmethod calculate-amount-hidden-content-right ((w buffered-window))
  (apply #'max
         (or (loop
                for nil across (subseq (buffer w) (buffer-offset-y w))
                for ilin
                from (buffer-offset-y w)
                to (+ (buffer-offset-y w) (- (height w) 3))
                collect (- 
                         (length (elt (buffer w) ilin))
                         (buffer-offset-x w)
                         (- (width w) 2)))
             '(0))))

(defmethod calculate-longest-line-shown ((w buffered-window))
  (apply #'max
         (or (loop
                for ilin
                from (buffer-offset-y w)
                to (+ (buffer-offset-y w) (- (height w) 3))
                collect (length (elt (buffer w) ilin)))
             '(0))))

(defmethod scroll-up ((w buffered-window))
  (when (> (buffer-offset-y w) 0)
    (decf (buffer-offset-y w))))

(defmethod scroll-down ((w buffered-window))
  (when (> (- (length (buffer w)) 
              (buffer-offset-y w))
           (- (height w) 2))
    (incf (buffer-offset-y w))))

(defmethod scroll-left ((w buffered-window))
  (when (> (buffer-offset-x w) 0)
    (decf (buffer-offset-x w))))

(defmethod scroll-right ((w buffered-window))
  (when (> (calculate-amount-hidden-content-right w) 0)
    (incf (buffer-offset-x w))))

(defmethod scroll-up-to-begining ((w buffered-window))
  (setf (buffer-offset-y w) 0))

(defmethod scroll-to-left-begining ((w buffered-window))
  (setf (buffer-offset-x w) 0))

(defmethod scroll-down-to-end ((w buffered-window))
  (when (> (length (buffer w))
           (- (height w) 2))
    (setf (buffer-offset-y w)
          (- (length (buffer w))
             (- (height w) 2)))))

(defmethod scroll-to-right-end ((w buffered-window))
  (let ((longest-line-shown (calculate-longest-line-shown w)))
    (when (> longest-line-shown (- (width w) 2))
      (setf (buffer-offset-x w) (- longest-line-shown (- (width w) 2))))))

(defclass selector-window (buffered-window)
  ((currently-selected-row
    :initarg :currently-selected-row
    :accessor currently-selected-row
    :initform 1)))

(defgeneric repaint (object &key)
  (:documentation "Repaints an object."))

(defmethod repaint ((w window) &key (do-initialization nil))
  (when do-initialization
    (loop for ifunc in (sort (copy-seq (init-hook w))
                             #'<
                             :key #'second)
       do
         (when *debug*
           (format t "Will execute init function: ~A (Prio ~A) for Window ~A~%"
                   (car ifunc) (second ifunc)
                   (name w)))
         (funcall (car ifunc) w))
    (mark-for-later-refresh w))
  (loop for ifunc in (sort (copy-seq (meta-hook w))
                           #'<
                           :key #'second)
     do
       (when *debug*
         (format t "Will execute meta function: ~A (Prio ~A) for Window ~A~%"
                 (car ifunc) (second ifunc) (name w)))
       (funcall (car ifunc) w))
  (mark-for-later-refresh w))

#+nil(defmethod repaint :around ((w centered-window) &key)
                (when (and (> (+ 2 (height *screen*))
                              (height w))
                           (> (+ 2 (width *screen*))
                              (width w)))
                  (setf (begin-x w)
                        (round (/ (- (width *screen*) (width w)) 2))
                        (begin-y w)
                        (round (/ (- (height *screen*) (height w)) 2))))
                (call-next-method))

#+nil(defmethod repaint :around ((w resizable-window) &key)
                (multiple-value-bind (begin-x begin-y width height)
                    (calculate-window-dimensions :start-left-dist (start-left-dist w)
                                                 :start-top-dist (start-top-dist w)
                                                 :start-bottom-dist (start-bottom-dist w)
                                                 :start-right-dist (start-right-dist w)
                                                 :end-left-dist (end-left-dist w)
                                                 :end-top-dist (end-top-dist w)
                                                 :end-bottom-dist (end-bottom-dist w)
                                                 :end-right-dist (end-right-dist w))
                  (when (and (> height 0)
                             (> width 0))
                    (setf (begin-x w) begin-x
                          (begin-y w) begin-y
                          (width w) width
                          (height w) height)))
                (call-next-method))

#+nil(defmethod repaint :around ((w window) &key redo-handler)
                (when redo-handler
                  (when (and (slot-boundp w 'curses-handler)
                             (curses-handler w))
                    (destroy w))
                  (setf (curses-handler w) (charms/ll:newwin
                                            (height w) (width w) 
                                            (begin-y w) (begin-x w))))
                (when (draw-func w)
                  (funcall (draw-func w) w))
                (call-next-method))

#+nil(defmethod repaint :after ((w window) &key)
                ;; dirty. We really want widgets be painted at last (f.e. after
                ;; decoration/borders)
                (when (and (typep w 'widgeted-window)
                           (active-widget w))
                  (loop for iwidget in (widget-stack w)
                     do
                       (repaint iwidget))
                  (mark-for-later-refresh w)) ; we need an explizit
                                        ; mark-for-later-refresh, as this
                                        ; happens after primary repaint method
                ;; dirty hack to get the cursor in widgets on buffered-windows
                ;; finally, as late as possible we draw the inverse cursor
                ;; (when (and (typep w 'buffered-window)
                ;;            (typep w 'widgeted-window)
                ;;            (active-widget w)
                ;;            (typep (active-widget w) 'input-line-widget))
                ;;   (with-wattr (active-window *screen*)
                ;;       (charms/ll:a_reverse)
                ;;     (wput w
                ;;           (+ (x-pos-cursor (active-widget w))
                ;;              (x (active-widget w)))
                ;;           (y (active-widget w))
                ;;           (char (concatenate 'string
                ;;                              (safe-subseq
                ;;                               (content (active-widget w))
                ;;                               (x-offset (active-widget w)))
                ;;                              " ")
                
                ;;                 (x-pos-cursor (active-widget (active-window *screen*))))
                ;;           :raw t))
                ;;   (mark-for-later-refresh w))
                )

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; print / put

(defgeneric wprint (window x y string &key raw))

(defmethod wprint ((w window) x y string &key raw)
  (declare (ignore raw))
  (when (equal x :centered)
    (let ((xpos (/ (- (width w) (length string)) 2)))
      (if (> xpos 0)
          (setf x (floor xpos))
          (setf x 0))))
  (when (> (1- (length string)) (- (width w) x))
    (setf string (subseq string 0 (if (< (- (width w) x) 0)
                                      0
                                      (- (width w) x)))))
  (charms/ll:mvwaddstr (curses-handler w) y x
                       (format nil "~A" string)))

(defmethod wprint ((w buffered-window) x y string &key raw)
  (if raw                
      (call-next-method) ; raw calls the nativ wprint (unbuffered)
      (progn
        (loop
           for ix = x then (incf ix)
           for ichar across string
           do
             (wput w ix y ichar)))))

(defgeneric wput (window x y string &key raw))

(defmethod wput ((w window) x y char &key raw)
  (declare (ignore raw))
  (charms/ll:mvwaddch
   (curses-handler w) y x
   (typecase char
     (integer char)
     (character (char-code char))
     (string (char-code (char char 0))))))

(defmethod wput ((w buffered-window) x y char &key raw)
  (if raw
      (progn
        (setf x (- x (buffer-offset-x w)))
        (setf y (- y (buffer-offset-y w)))
        (call-next-method w x y char)) 
      (progn             
        (decf x) (decf y) ; we decrement x/y to countervail the border
                                        ; ("fixed" windows start their coordinates,
                                        ; including the borders, by 0).
        (when (< (length (buffer w))
                 (1+ y))
          (loop for i = (vector-push-extend "" (buffer w))
             while (< (length (buffer w))
                      (1+ y))))
        (let ((line-to-change (elt (buffer w) y)))
          (when (< (length line-to-change)
                   (1+ x))
            (loop for i = (setf line-to-change
                                (concatenate 'string
                                             line-to-change
                                             " "))
               while
                 (< (length line-to-change)
                    (1+ x)) ))
          (setf (elt line-to-change x)
                (typecase char
                  (integer (code-char char))
                  (character char)
                  (string (char char 0))))
          (setf (elt (buffer w) y)
                line-to-change)))))

(defmethod wprint-append ((w buffered-window) row)
  "Appends a row to a buffered-window."
  (vector-push-extend row (buffer w)))






