;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Events

(in-package :tui)

(defvar *last-executed-event* nil
  "The last function-name of the last executed event.")

(eval-when (:execute :load-toplevel :compile-toplevel)
  (defun %addkey (keymap keychord function)
    (if (assoc (first keychord) keymap)
        (assoc (first keychord) keymap)
        (push (list (first keychord) nil) keymap))
    (if (second keychord)
        (progn (unless
                   (listp (second (assoc (first keychord) keymap)))
                 (setf (second (assoc (first keychord) keymap)) nil))
               (if (assoc (second keychord)
                          (second (assoc (first keychord) keymap)))
                   (setf (second (assoc (second keychord)
                                        (second (assoc (first keychord)
                                                       keymap))))
                         function)
                   (push (list (second keychord) function)
                         (second (assoc (first keychord) keymap)))))
        (setf (second (assoc (first keychord) keymap)) function))
    keymap))

(defmacro addkey (keymap keychord function)
  (if (equal (car keychord) 'or)
      `(progn
         ,@(loop for ikeychord in (cdr keychord)
                 collect
                 `(setf ,keymap (%addkey ,keymap (quote ,ikeychord) (quote ,function)))))
      `(setf ,keymap (%addkey ,keymap (quote ,keychord) (quote ,function))) ))

(defmacro defkeymap (keymap keyfunc-parameters &rest defs)
  `(progn
     (defparameter ,keymap nil)
     ,@(loop
         for i in defs
         for funcname = (intern
                         (string-upcase
                          (format nil "~A-~A" keymap (second i))))
         collect
         `(progn
            (addkey ,keymap ,(first i) ,funcname)
            ,(if (third i)
                 `(defun ,funcname ,keyfunc-parameters
                    (declare (ignorable ,@keyfunc-parameters))
                    ,@(nthcdr 2 i)
                    (setf *last-executed-event* ',funcname)))))))

(define-condition exit-event-loop ()
  ())

(defmacro event-loop (&key func-before-event
                        func-inner-event
                        func-after-event custom-event-map
                        (exit-condition 'exit-event-loop))
  `(progn
     (repaint *screen*)
     (do-update)
     (handler-case
         (loop
           named event-loop
           do
              (when ,func-before-event
                (funcall ,func-before-event))
              (cond
                ;; do we supply a custom (window/widget independent
                ;; event-map?
                (,custom-event-map
                 (handle-event-map ,custom-event-map
                                   ,func-inner-event))
                ;; is there a widget active, lets combine the
                ;; event-maps in 'event-maps' and handle them.
                ((and (typep (active-window *screen*) 'widgeted-window)
                      (active-widget (active-window *screen*))
                      (tui:event-maps (active-widget
                                       (active-window *screen*))))
                 (handle-event-map
                  (let ((result nil))
                    (loop for ilist in (tui:event-maps
                                        (active-widget
                                         (active-window *screen*)))
                          do
                             (setf result (append ilist result)))
                    result)
                  ,func-inner-event))
                ;;is there a widget active, deprecated, handle the
                ;;'event-map' (should use event-maps in future).
                ((and (typep (active-window *screen*) 'widgeted-window)
                      (active-widget (active-window *screen*)))
                 (handle-event-map (event-map
                                    (active-widget (active-window *screen*)))
                                   ,func-inner-event))
                ;; otherwise handle event-map of active window
                (t 
                 (handle-event-map (event-map (active-window *screen*))
                                   ,func-inner-event)))
              (when ,func-after-event
                (funcall ,func-after-event))
              (if (need-full-repaint *screen*)
                  (progn ;; (dslime "need for full repaint?")
                    (repaint *screen*)
                    (setf (need-full-repaint *screen*) nil))
                  (progn
                    (repaint (active-window *screen*))
                    (do-update))))
       (,exit-condition nil))))

(defun handle-event-map (event-map &optional func-inner-event)
  (let ((key (loop
               for fetched-key = (charms/ll:getch)
               while (= charms/ll:key_resize fetched-key)
               do
                  (repaint *screen*)
                  (do-update)
               finally
                  (return fetched-key))))
    (loop for ikey in event-map
          do
             (when (if (atom (car ikey))
                       (= (car ikey) key)
                       (and (>= key (car (car ikey)))
                            (<= key (second (car ikey)))))
               (if (atom (second ikey))
                   (progn
                     (funcall (second ikey) key)
                     (when func-inner-event ; call inner event function
                       (funcall func-inner-event))
                     ;; (repaint (active-window *screen*))
                     (do-update))
                   (handle-event-map (second ikey) func-inner-event))))))

