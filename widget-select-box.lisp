(in-package :tui)

(defkeymap *select-box-event-map*
    (key)
  ((or (#. (char-code #\tab))
       (#. charms/ll:key_right)
       (#. charms/ll:key_down)
       (#. (char-code #\So)))
   next-widget
   (select-next-widget (active-window *screen*)))
  
  ((or (#. charms/ll:key_btab)
       (#. charms/ll:key_left)
       (#. charms/ll:key_up)
       (#. (char-code #\Dle)))
   prev-widget
   (select-previous-widget (active-window *screen*)))

  ((or (#. (char-code #\Space))
       (#. (char-code #\x)))
   toggle-box
   (aif (selected-p (active-widget (active-window *screen*)))
        (setf (selected-p (active-widget (active-window *screen*))) nil)
        (setf (selected-p (active-widget (active-window *screen*))) t))
   (when (func-to-call-on-act (active-widget (active-window *screen*)))
     (funcall (func-to-call-on-act (active-widget (active-window *screen*)))
              (active-widget (active-window *screen*))))))

(defclass select-box (widget)
  ((content
    :initarg :content
    :accessor content
    :initform "not set")
   (selected-p
    :initarg :selected-p
    :accessor selected-p
    :initform nil)
   (event-maps
    :initarg :event-maps
    :accessor event-maps
    :initform (list *select-box-event-map*))))

(defmethod repaint ((wi select-box) &key)
  (if (selected-p wi)
      (if (active-p wi)
          (with-wattr (mother-window wi) (charms/ll:a_reverse)
            (wprint (mother-window wi) (x wi) (y wi)
                    (format nil "[x] ~A" (content wi))))
          (wprint (mother-window wi) (x wi) (y wi)
                  (format nil "[x] ~A" (content wi))))
      (if (active-p wi)
          (with-wattr (mother-window wi) (charms/ll:a_reverse)
            (wprint (mother-window wi) (x wi) (y wi)
                    (format nil "[ ] ~A" (content wi))))
          (wprint (mother-window wi) (x wi) (y wi)
                  (format nil "[ ] ~A" (content wi))))))
