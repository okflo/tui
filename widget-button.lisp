(in-package :tui)

(defkeymap *button-event-map*
    (key)
  ((#. (char-code #\Newline))
   leave-input-line-mode
   (when (func-to-call-on-act (active-widget (active-window *screen*)))
     (funcall (func-to-call-on-act (active-widget (active-window *screen*)))
              (active-widget (active-window *screen*)))))

  ((or (#. (char-code #\tab))
       (#. charms/ll:key_right)
       (#. charms/ll:key_down)
       (#. (char-code #\So)))
   next-widget
   (select-next-widget (active-window *screen*)))

  ((or (#. charms/ll:key_btab)
       (#. charms/ll:key_left)
       (#. charms/ll:key_up)
       (#. (char-code #\Dle)))
   prev-widget
   (select-previous-widget (active-window *screen*))))

(defclass button (widget)
  ((content
    :initarg :content
    :accessor content
    :initform "Button")
   (event-maps
    :initarg :event-maps
    :accessor event-maps
    :initform (list *button-event-map*))))

(defmethod repaint ((wi button) &key)
  (if (active-p wi)
      (with-wattr (mother-window wi) (charms/ll:a_reverse)
        (wprint (mother-window wi) (x wi) (y wi)
                (format nil "[ ~A ]" (content wi))))
      (wprint (mother-window wi) (x wi) (y wi)
              (format nil "[ ~A ]" (content wi)))))
