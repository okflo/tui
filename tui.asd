(in-package :cl-user)

(asdf:defsystem :tui
  :version "0.1"
  :depends-on #+(or ecl clisp) (:cl-charms :cl-ppcre)
  #+(or sbcl ccl) (:cl-charms :cl-ppcre :swank)
  :components ((:file "packages")
               (:file "utilities"
                :depends-on ("packages"))
               (:file "screen"
                :depends-on ("utilities" "packages"))
               (:file "windows"
                :depends-on ("screen" "utilities" "packages"))
               (:file "events"
                :depends-on ("windows" "screen" "utilities" "packages"))
               (:file "widgets"
                :depends-on ("utilities" "packages" "windows" "screen"))
               (:file "widget-text-field"
                :depends-on ("widgets" "utilities" "packages" "windows" "screen"))
               (:file "widget-button"
                :depends-on ("widgets" "utilities" "packages" "windows" "screen"))
               (:file "widget-select-box"
                :depends-on ("widgets" "utilities" "packages" "windows" "screen"))
               (:file "widget-radio-button"
                :depends-on ("widgets" "utilities" "packages" "windows" "screen"))
               (:file "widget-text-input"
                :depends-on ("utilities" "packages" "windows" "screen"))
               (:file "menu"
                :depends-on ("widgets" "utilities" "packages" "windows"
                                       "screen"))
               (:file "tests"
                :depends-on ("utilities" "windows" "widgets" "packages"
                                         "screen"))
               (:file "tests-widgets"
                :depends-on ("utilities" "windows" "widgets" "packages"
                                         "screen" "widget-text-input"))))

