(in-package :tui)

(defmacro aif (test then &optional else)
  `(let ((it ,test))
     (if it ,then ,else)))

(defun safe-subseq (sequence start &optional (end (length sequence)))
  (cond ((and (< start (length sequence))
	      (< end (length sequence)))
	 (subseq sequence start end))
	((and (>= start (length sequence))
	      (>= end (length sequence)))
	 "")
	((< start 0)
	 "")
	((< start (length sequence))
	 (subseq sequence start))
	((< end 0)
	 "")))

(defun insert-char (string thing-to-insert position)
  (let ((left (subseq string 0 position))
	(right (subseq string position)))
    (concatenate 'string left (typecase thing-to-insert
				(character (string thing-to-insert))
				(string thing-to-insert)) right)))

(defun string-fixed-size (string size)
  (concatenate 'string
               string
               (with-output-to-string (s)
                 (loop for i from 1 to (- size (length string))
                    do
                      (princ " " s)))))

(defun remove-char (string position)
  (concatenate 'string
	       (subseq string 0 (1- position))
	       (subseq string position)))

(defun length-of-element-in-list (list func)
  (apply func
	 (mapcar #'(lambda (x)
		     (if (typep x 'string)
			 (length x)
			 0))
                 list)))   
