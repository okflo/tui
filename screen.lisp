(in-package :tui)

(defvar *screen* nil)

(defclass screen ()
  ((curses-handler
    :accessor curses-handler
    :initform nil
    :documentation "Handler to the actual ncurses screen-pointer.")
   (width
    :accessor width)
   (height
    :accessor height)
   (active-window
    :accessor active-window
    :initform nil
    :documentation "Points to the currently active window.")
   (window-stack
    :accessor window-stack
    :initform ()
    :documentation "List with the windows put on the screen.")
   (need-full-repaint
    :accessor need-full-repaint
    :initform t)))

(defmethod update-size ((screen screen))
  (let (width height)
    (charms/ll:getmaxyx (curses-handler screen) height width)
    (setf (height screen) height)
    (setf (width screen) width)))

(defmethod destroy ((screen screen))
  ;; (charms/ll:erase)
  (charms/ll:endwin))

(defmethod clear ((s screen))
  (charms/ll:clear))

(defun init-screen ()
  (setf *screen* (make-instance 'screen))
  (setf (curses-handler *screen*) (charms/ll:initscr))
  (when (cffi:null-pointer-p (curses-handler *screen*))
    (error "Initialize (curses handler is null pointer) failed. "))
  (update-size *screen*)
  (charms/ll:noecho)
  (charms/ll:cbreak)
  (charms/ll:raw)
  (charms/ll:curs-set 0)
  (charms/ll:keypad (curses-handler *screen*) charms/ll:TRUE)
  (charms/ll:start-color)
  ;; cursor
  (charms/ll:init-pair 1 charms/ll:COLOR_WHITE charms/ll:COLOR_RED)
  ;; unfocused Window Borders
  (charms/ll:init-pair 2 charms/ll:COLOR_BLUE charms/ll:COLOR_BLACK)
  ;; Status Window
  (charms/ll:init-pair 3 charms/ll:COLOR_WHITE charms/ll:COLOR_BLACK)
  ;; breakpoints
  (charms/ll:init-pair 4 charms/ll:COLOR_WHITE charms/ll:COLOR_GREEN)
  ;; text-field-widget
  (charms/ll:init-pair 5 charms/ll:COLOR_WHITE charms/ll:COLOR_BLUE)
  ;; search-result
  (charms/ll:init-pair 6 charms/ll:COLOR_WHITE charms/ll:COLOR_YELLOW)
  ;; ip
  (charms/ll:init-pair 7 charms/ll:COLOR_WHITE charms/ll:COLOR_MAGENTA)
  (charms/ll:clear)
  (charms/ll:refresh))

(defun do-update ()
  (charms/ll:doupdate))

(defun nodelay (on/off)
  (if on/off
      (charms/ll:nodelay (curses-handler *screen*) 1)
      (charms/ll:nodelay (curses-handler *screen*) 0)))

(defmethod repaint ((s screen) &key)
  (when (and (slot-boundp s 'curses-handler)
             (curses-handler s))
    (destroy *screen*))
  (setf (curses-handler *screen*) (charms/ll:initscr))
  (update-size *screen*)
  (charms/ll:noecho)
  (charms/ll:cbreak)
  (charms/ll:raw)
  (charms/ll:curs-set 0)
  (charms/ll:keypad (curses-handler *screen*) charms/ll:TRUE)
  (charms/ll:start-color)
  (charms/ll:init-pair 1 charms/ll:COLOR_WHITE charms/ll:COLOR_RED)
  ;; unfocused Window Borders
  (charms/ll:init-pair 2 charms/ll:COLOR_BLUE charms/ll:COLOR_BLACK)
  ;; Status Window
  (charms/ll:init-pair 3 charms/ll:COLOR_WHITE charms/ll:COLOR_BLACK)
  (clear *screen*)
  (charms/ll:refresh)
  (loop for iwindow in (reverse (window-stack s))
     do
       (repaint iwindow :do-initialization t))
  (do-update))

(defmacro with-tui (&body body)
  `(unwind-protect
        (progn
          (init-screen)
          ,@body)
     (progn
       (loop for iwindow in (reverse (window-stack *screen*))
          do
            (destroy iwindow))
       (destroy *screen*))))

(defun start-wattr (window attr)
  (charms/ll:wattron (curses-handler window) attr))

(defun stop-wattr (window attr)
  (charms/ll:wattroff (curses-handler window) attr))

(defmacro with-wattr (window list-of-attrs &body body)
  `(progn
     ,@(loop for iattr in list-of-attrs
          collect
            `(charms/ll:wattron (curses-handler ,window) ,iattr))
     ,@body
     ,@(loop for iattr in (reverse list-of-attrs)
          collect
            `(charms/ll:wattroff (curses-handler ,window) ,iattr))))

(defparameter *color-attributes* nil)
(defparameter *color-attributes-counter* 1)

(defun create-color-combination (name fg-color bg-color)
  )
