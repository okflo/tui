(in-package :tui)

(defkeymap *menu-keymap*
    (key)
  ((or (#. charms/ll:key_right)
       (#. (char-code #\Ack)))
   right
   (when (< (active-menubar-entry (active-widget (active-window *screen*)))
            (1- (length (menu-structure (active-widget (active-window *screen*))))))
     (incf (active-menubar-entry (active-widget (active-window *screen*))))
     (setf (active-dropdown-entry (active-widget (active-window *screen*))) 0))
   ;; (setf (need-full-repaint *screen*) t)
   )
  
  ((or (#. charms/ll:key_left)
       (#. (char-code #\Stx)))
   left
   (when (> (active-menubar-entry (active-widget (active-window *screen*)))
            0)
     (decf (active-menubar-entry (active-widget (active-window *screen*))))
     (setf (active-dropdown-entry (active-widget (active-window *screen*))) 0))
   ;; (setf (need-full-repaint *screen*) t) ;; we don't need this,
   ;; just flickers the screen?
   )
  
  ((or (#. charms/ll:key_down)
       (#. (char-code #\So)))
   down
   (when (< (active-dropdown-entry (active-widget (active-window *screen*)))
            (- (length (nth (active-menubar-entry
                             (active-widget
                              (active-window *screen*)))
                            (menu-structure
                             (active-widget
                              (active-window *screen*)))))
               2))
     (incf (active-dropdown-entry (active-widget (active-window *screen*)))
           (if (equal
                '-
                (car
                 (nth
                  (1+ (active-dropdown-entry (active-widget (active-window *screen*))))
                  (cdr
                   (nth (active-menubar-entry
                         (active-widget
                          (active-window *screen*)))
                        (menu-structure
                         (active-widget
                          (active-window *screen*))))))))
               2 1))))
  
  ((or (#. charms/ll:key_up)
       (#. (char-code #\Dle)))
   up
   (when (> (active-dropdown-entry (active-widget (active-window *screen*)))
            0)
     (decf (active-dropdown-entry (active-widget (active-window *screen*)))
           (if (equal
                '-
                (car
                 (nth
                  (1- (active-dropdown-entry (active-widget (active-window *screen*))))
                  (cdr
                   (nth (active-menubar-entry
                         (active-widget
                          (active-window *screen*)))
                        (menu-structure
                         (active-widget
                          (active-window *screen*))))))))
               2 1))))
  
  ((or (#. (char-code #\Bel))
       (#. (char-code #\Esc) #. (char-code #\m))
       (#. (char-code #\Esc) #. (char-code #\Esc)))
   leave-input-line-mode
   (deactivate-widget (active-window *screen*))
   ;; (setf (need-full-repaint *screen*) t)
   )
  ;; select entry
  ((#. (char-code #\Newline))
   select
   (aif (third
         (nth
          (active-dropdown-entry (active-widget (active-window *screen*)))
          (cdr
           (nth (active-menubar-entry
                 (active-widget
                  (active-window *screen*)))
                (menu-structure
                 (active-widget
                  (active-window *screen*)))))))
        (progn
          (deactivate-widget (active-window *screen*))
          (funcall it)))))

(defclass menu (widget)
  ((x :initform 1)
   (y :initform 1)
   (menu-structure
    :initarg :menu-structure
    :accessor menu-structure
    :initform nil)
   (active-menubar-entry
    :initarg :active-menubar-entry
    :accessor active-menubar-entry
    :initform 0)
   (active-dropdown-entry
    :initarg :active-dropdown-entry
    :accessor active-dropdown-entry
    :initform 0)
   (event-map
    :initarg :event-map
    :accessor event-map
    :initform *menu-keymap*)))

(defmethod repaint ((m menu) &key)
  (when (active-p m)
    (wprint (mother-window m)           ; clear momentary part of
                                        ; first line
            (x m) (y m)
            (with-output-to-string (s)
              (loop for i from 1 to (reduce #'+
                                            (loop for ii in (menu-structure m)
                                                  collect (+ (length (car ii)) 2)))
                    do
                       (princ " " s))) :raw t)
    (loop
      for im in (menu-structure m)     ; menubar-entry and following
                                        ; menu-entries
      for n = 0 then (incf n) ; number of menubar-entry/menu-entries
      with nn = 2 ; x-coordinate of menubar and following entries
      do
         (if (= (active-menubar-entry m) n) ; is this menu open?
             (progn                         ; - yes
               (with-wattr (mother-window m) (charms/ll:a_reverse)
                 (wprint (mother-window m)
                         (1- nn) (y m)
                         (concatenate 'string
                                      " "
                                      (car im)
                                      " ")
                         :raw t)) ; invert menubar-entry
               (let ((menu-width (+ (length-of-element-in-list 
                                     (loop for i in (cdr im)
                                           collect (car i))
                                     #'max)
                                    15)) 
                     (nn (1- nn)))  ; reduce drawing line of open menu
                                        ; by 1 to align it with the
                                        ; menubar-entry
                 (loop
                   for isub in (cdr im) ; menu without menubar-entry
                   for line = (1+ (y m)) then (incf line) ; current position
                   for number-of-dropdown-entry = 0
                     then (incf number-of-dropdown-entry) ; count the entries
                   do
                      (wprint (mother-window m)
                              nn line
                              #+sbcl"|"; "│"
                              #+ccl"|"
                              #+ecl"|"
                              #+clisp"|"
                              :raw t)
                      (when (and (active-dropdown-entry m)
                                 (= (active-dropdown-entry m) number-of-dropdown-entry))
                        (start-wattr (mother-window m) charms/ll:a_reverse))
                      (if (equal (car isub) '-) ; paint divider line
                          (wprint (mother-window m)
                                  (1+ nn) line
                                  (format nil "~{~A~}"
                                          (loop for i from 1 to menu-width collect 
                                                #+sbcl"-";"─"
                                                #+ccl"-"
                                                #+ecl"-"
                                                #+clisp"-"))
                                  :raw t)
                          (wprint (mother-window m) ; otherwise paint menu-entry
                                  (1+ nn) line
                                  ;; (format nil ; actual menu-entry-string
                                  ;;         (format nil "~~~A,1,2<~~A~~;~~A~~>" ; make
                                  ;;                 menu-width)                 ; formatstring
                                  ;;         (car isub)
                                  ;;         (aif (second isub)
                                  ;;              it " "))
                                  (format nil ; actual menu-entry-string
                                          "~V,1,2<~A~;~A~>"
                                          menu-width
                                          (car isub)
                                          (aif (second isub)
                                               it " "))
                                  :raw t
                                  ))
                      (when (and (active-dropdown-entry m)
                                 (= (active-dropdown-entry m) number-of-dropdown-entry))
                        (stop-wattr (mother-window m) charms/ll:a_reverse))
                      (wprint (mother-window m)
                              (+ nn menu-width 1) line
                              #+sbcl"|";(string #\BOX_DRAWINGS_LIGHT_VERTICAL)
                              #+ccl"|"
                              #+ecl"|"
                              #+clisp"|"
                              :raw t)
                   finally
                      (wprint (mother-window m) ; paint ending line
                              nn (1+ line)
                              (format nil
                                      #+sbcl"+~{~A~}+";"└~{~A~}┘"
                                      #+ccl"+~{~A~}+"
                                      #+ecl"+~{~A~}+"
                                      #+clisp"+~{~A~}+"
                                      (loop
                                        for i from 1 to menu-width
                                        collect 
                                        #+sbcl"-"; "─"
                                        #+ccl"-"
                                        #+ecl"-"
                                        #+clisp"-"))
                              :raw t))))

             (progn
               (wprint (mother-window m)
                       nn (y m)
                       (car im)
                       :raw t)))
         (incf nn (+ (length (car im)) 2)))))
