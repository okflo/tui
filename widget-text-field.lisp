(in-package :tui)

;;; simple-text-field
;; VERY simple textfield, just enter text, no editing of text

(defkeymap *simple-text-field-eventmap*
    (key)
  (((32 126)) enter-char
              (when (< (1- (length (content (active-widget (active-window *screen*)))))
                       (width (active-widget (active-window *screen*))))
                (setf (content (active-widget (active-window *screen*)))
                      (concatenate 'string
                                   (content (active-widget (active-window *screen*)))
                                   (string (code-char key))))))
  ((#. charms/ll:key_backspace) backspace
                                (when (> (length (content (active-widget (active-window *screen*))))
                                         0)
                                  (setf (content (active-widget (active-window *screen*)))
                                        (subseq  (content (active-widget (active-window *screen*)))
                                                 0
                                                 (- (length (content (active-widget (active-window *screen*))))
                                                    1)))))
  ((#. (char-code #\Newline)) leave-widget
                              (deactivate-widget (active-window *screen*))
                              (repaint *screen*)))

(defclass simple-text-field (widget)
  ((width :initarg :width :accessor width :initform 8)
   (content :initarg :content :accessor content :initform "")
   (event-maps :initarg :event-maps :accessor event-maps
               :initform (list *simple-text-field-eventmap*))))

(defmethod repaint ((wi simple-text-field) &key)
  (if (active-p wi)
      (progn
        (wprint (mother-window wi)
                (x wi) (y wi)
                (content wi))
        (with-wattr (mother-window wi) (charms/ll:a_reverse)
          (wprint (mother-window wi)
                  (+ (x wi) (length (content wi))) (y wi)
                  " "))
        (wprint (mother-window wi)
                (+ (x wi) (length (content wi)) 1) (y wi)
                " "))
      (wprint (mother-window wi)
              (x wi) (y wi)
              (string-fixed-size (content wi) (width wi))))
  (mark-for-later-refresh (mother-window wi)))

(defkeymap *text-field-event-map*
    (key)

  (((32 126))
   insert-char
   (let ((wi (active-widget (active-window *screen*))))
     (let ((temp-content (copy-seq (content wi))))
       (setf temp-content
             (insert-char temp-content
                          (code-char key)
                          (+ (x-cursor-pos wi)
                             (x-offset wi))))
       (when (and (cl-ppcre:scan (validate-regex wi)
                                 temp-content)
                  (<= (length temp-content)
                      (max-content-length wi)))
         (setf (content wi) temp-content)
         (*text-field-event-map*-cursor-right key)))))

  ((#. (char-code #\Eot))
   delete-char
   (let ((wi (active-widget (active-window *screen*))))
     (when (> (length (content wi))
              (+ (x-cursor-pos wi)
                 (x-offset wi)))
       (setf (content wi)
             (remove-char
              (content wi)
              (+ (x-cursor-pos wi)
                 (x-offset wi)
                 1))))))
  
  ((or (#. (char-code #\Ack))
       (#. charms/ll:key_right))
   cursor-right
   (let ((wi (active-widget (active-window *screen*))))
     (cond
       ((and (< (x-cursor-pos wi)
                (1- (width wi)))
             (< (+ (x-cursor-pos wi)
                   (x-offset wi))
                (length (content wi))))
        (incf (x-cursor-pos wi)))
       ((and (= (x-cursor-pos wi)
                (1- (width wi)))
             (< (+ (x-offset wi)
                   (width wi))
                (1+ (length (content wi)))))
        (incf (x-offset wi))))))

  ((or (#. (char-code #\Stx))
       (#. charms/ll:key_left))
   cursor-left
   (let ((wi (active-widget (active-window *screen*))))
     (cond
       ((> (x-cursor-pos wi) 0)
        (decf (x-cursor-pos wi)))
       ((and (= (x-cursor-pos wi) 0)
             (> (x-offset wi) 0))
        (decf (x-offset wi))))))

  ((#. charms/ll:key_backspace)
   backspace-key
   (let ((wi (active-widget (active-window *screen*))))
     (when (> (+ (x-cursor-pos wi)
                 (x-offset wi))
              0)
       (setf (content wi)
             (remove-char
              (content wi)
              (+ (x-cursor-pos wi)
                 (x-offset wi))))
       (if (and (= (+ (x-offset wi) (width wi)) (+ (length (content wi)) 2))
                (> (x-offset wi) 0))
           (decf (x-offset wi))
           (decf (x-cursor-pos wi))))))

  ((or (#. (char-code #\tab))
       (#. charms/ll:key_down)
       (#. (char-code #\So)))
   next-widget
   (select-next-widget (active-window *screen*)))
  
  ((or (#. charms/ll:key_btab)
       (#. charms/ll:key_up)
       (#. (char-code #\Dle)))
   prev-widget
   (select-previous-widget (active-window *screen*)))

  ((#. (char-code #\Newline))
   leave-input-line-mode
   (deactivate-widget (active-window *screen*))))

(defclass text-field (widget)
  ((width
    :initarg :width
    :accessor width
    :initform nil)
   (content
    :initarg :content
    :accessor content
    :initform nil)
   (max-content-length
    :initarg :max-content-length
    :accessor max-content-length
    :initform 500)
   (validate-regex
    :initarg :validate-regex
    :accessor validate-regex
    :initform ".*")
   (x-cursor-pos
    :initarg :x-cursor-pos
    :accessor x-cursor-pos
    :initform 0)
   (x-offset
    :initarg :x-offset
    :accessor x-offset
    :initform 0)
   (event-maps
    :initarg :event-maps
    :accessor event-maps
    :initform (list *text-field-event-map*))))

(defmethod repaint ((wi text-field) &key)
  (if (active-p wi)
      (progn
        (wprint (mother-window wi)
                (x wi) (y wi)
                (format nil "|~A|"
                        (string-fixed-size
                         (safe-subseq (concatenate 'string
                                                   (content wi)
                                                   " ")
                                      (x-offset wi) (+ (width wi)
                                                       (x-offset wi)))
                         (width wi))))
        (with-wattr (mother-window wi) (charms/ll:a_reverse)
          (wput (mother-window wi)
                (+ (x wi)
                   (x-cursor-pos wi)
                   1)
                (y wi)
                (subseq (concatenate 'string
                                     (content wi)
                                     " ")
                        (+ (x-offset wi) (x-cursor-pos wi))
                        (+ (x-offset wi) (x-cursor-pos wi) 1)))))
      (progn
        (if (> (length (content wi)) (width wi))
            (wprint (mother-window wi)
                    (x wi) (y wi)
                    (format nil "|~A>|"
                            (string-fixed-size
                             (safe-subseq (content wi) 0 (1- (width wi)))
                             (1- (width wi)))))
            (wprint (mother-window wi)
                    (x wi) (y wi)
                    (format nil "|~A|"
                            (string-fixed-size
                             (safe-subseq (content wi) 0 (width wi))
                             (width wi))))))))

(defclass auto-stretch-text-field (text-field)
  ((dist-right
    :initarg :dist-right
    :accessor dist-right
    :initform nil)))

(defmethod initialize-instance :after ((wi auto-stretch-text-field) &key)
  (let ((new-width (- (width (mother-window wi))
                      (x wi)
                      (dist-right wi)
                      2)))
    (setf (width wi)
	  (if (> new-width 0)
	      new-width
	      1))))

(defmethod repaint ((wi auto-stretch-text-field) &key)
  (let ((new-width (- (width (mother-window wi))
                      (x wi)
                      (dist-right wi)
                      2)))
    (setf (width wi)
	  (if (> new-width 0)
	      new-width
	      1)))
  (call-next-method))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; DEPRECATED

(defkeymap *text-field-widget-keymap*
    (key)
  (((32 126))
   insert-char
   (let ((wi (active-widget (active-window *screen*))))
     (setf (content wi)
	   (insert-char (content wi)
		        (code-char key)
		        (+ (x-pos-cursor wi)
			   (x-offset wi)))))
   (*text-field-widget-keymap*-cursor-right key))

  ((#. (char-code #\Eot))
   delete-char
   (when (> (length (content (active-widget (active-window *screen*))))
	    (+ (x-pos-cursor (active-widget (active-window *screen*)))
	       (x-offset (active-widget (active-window *screen*)))))
     (setf (content (active-widget (active-window *screen*)))
	   (remove-char
	    (content (active-widget (active-window *screen*)))
	    (+ (x-pos-cursor (active-widget (active-window *screen*)))
	       (x-offset (active-widget (active-window *screen*)))
	       1)))))
  
  ((#. charms/ll:key_backspace)
   backspace-key
   (when (> (+ (x-pos-cursor (active-widget (active-window *screen*)))
	       (x-offset (active-widget (active-window *screen*))))
	    0)
     (setf (content (active-widget (active-window *screen*)))
	   (remove-char
	    (content (active-widget (active-window *screen*)))
	    (+ (x-pos-cursor (active-widget (active-window *screen*)))
	       (x-offset (active-widget (active-window *screen*))))))
     (decf (x-pos-cursor (active-widget (active-window *screen*))))))
  ((#. (char-code #\Soh))
   jump-beginning
   (setf (x-pos-cursor (active-widget (active-window *screen*))) 0)
   (setf (x-offset (active-widget (active-window *screen*))) 0))
  ((#. (char-code #\Enq))
   jump-ending
   (if (> (length (content (active-widget (active-window *screen*))))
	  (width (active-widget (active-window *screen*))))
       (progn
	 (setf (x-pos-cursor (active-widget (active-window *screen*)))
	       (1-
		(width (active-widget (active-window *screen*)))))
	 (setf (x-offset (active-widget (active-window *screen*)))
	       (1+
		(- (length (content (active-widget (active-window *screen*))))
		   (width (active-widget (active-window *screen*)))))))
       (setf (x-pos-cursor (active-widget (active-window *screen*)))
	     (length (content (active-widget (active-window *screen*)))))))
  
  ((#. (char-code #\Ack))
   cursor-right
   (let ((wi (active-widget (active-window *screen*))))
     (cond
       ((and (< (x-pos-cursor wi)
	        (1- (width wi)))
	     (< (+ (x-pos-cursor wi)
		   (x-offset wi))
	        (length (content wi))))
        (incf (x-pos-cursor wi)))
       ((and (= (x-pos-cursor wi)
     	        (1- (width wi)))
	     (< (+ (x-offset wi)
		   (width wi))
	        (1+ (length (content wi)))))
        (incf (x-offset wi))))))
  
  ((#. (char-code #\Stx))
   cursor-left
   (let ((wi (active-widget (active-window *screen*))))
     (cond
       ((> (x-pos-cursor wi) 0)
        (decf (x-pos-cursor wi)))
       ((and (= (x-pos-cursor wi) 0)
	     (> (x-offset wi) 0))
        (decf (x-offset wi))))))

  ((#. (char-code #\Newline))
   leave-input-line-mode
   (deactivate-widget (active-window *screen*)))
  ((#. (char-code #\tab))
   next-widget
   (select-next-widget (active-window *screen*)))
  ((#. charms/ll:key_btab)
   prev-widget
   (select-previous-widget (active-window *screen*))))

(defclass text-field-widget (widget)
  ((width
    :initarg :width
    :accessor width)
   (content
    :initarg :content
    :accessor content
    :initform "")
   (x-offset
    :initarg :x-offset
    :accessor x-offset
    :initform 0)
   (x-pos-cursor
    :initarg :x-pos-cursor
    :accessor x-pos-cursor
    :initform 0)
   (event-map
    :initarg :event-map
    :accessor event-map
    :initform *text-field-widget-keymap*)))

(defmethod repaint ((wi text-field-widget) &key)
  (if (active-p wi)
      (progn
        (loop
          for i from 0 to (1- (width wi))
          for ch across (concatenate 'string
                                     (safe-subseq (content wi) (x-offset wi))
                                     "  ")
          do
             (if (= i (x-pos-cursor wi))
                 (progn
                   (with-wattr (mother-window wi) (charms/ll:a_reverse)
                     (wput (mother-window wi) (+ i (x wi)) (y wi) ch)))
                 (wput (mother-window wi) (+ i (x wi)) (y wi) ch))))
      (progn
	(wprint (mother-window wi)
		(x wi) (y wi)
		(safe-subseq (concatenate 'string (content wi)
                                          " ")
                             0 (width wi)))))
  (mark-for-later-refresh (mother-window wi)))

(defclass auto-stretch-text-field-widget (text-field-widget)
  ((dist-right
    :initarg :dist-right
    :accessor dist-right
    :initform nil)))

(defmethod initialize-instance :after ((wi auto-stretch-text-field-widget) &key)
  (let ((new-width (- (width (mother-window wi))
                      (x wi)
                      (dist-right wi))))
    (setf (width wi)
	  (if (> new-width 0)
	      new-width
	      1))))

(defmethod repaint ((wi auto-stretch-text-field-widget) &key)
  (let ((new-width (- (width (mother-window wi))
                      (x wi)
                      (dist-right wi))))
    (setf (width wi)
	  (if (> new-width 0)
	      new-width
	      1)))
  (call-next-method))
